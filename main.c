#include <stdio.h>
#include <string.h>
#include "account.h"
#include "admin.h"
#include "user.h"

Node LoadAccounts(FILE*);
void StoreAccounts(FILE*, Node);

int main()
{
	FILE* accFile;
	Node HEAD = NULL;

	accFile = fopen("CustomerData.txt", "r");
	HEAD = LoadAccounts(accFile);
	fclose(accFile);

	accFile = fopen("test.txt", "w");
	StoreAccounts(accFile, HEAD);
	fclose(accFile);

	PrintAccounts(HEAD);
	
	return 0;
}

/*************************************************************
 Name: LoadAccounts
 Purpose: Deserialize list from disk
 Parameters: FILE* input (pointer to input file)
 Return value: Node (Head of Data)
 Side Effects: None (Maybe lock/delete input file??)
*************************************************************/
Node LoadAccounts(FILE* input)
{
	Data info;
	Node HEAD = NULL;
	while (fscanf(input, "%s %s %u %s %d %s %lf",
			info.firstName,
			info.lastName,
			&info.state,
			info.phoneNumber,
			&info.id,
			info.passwordHash,
			&info.balance) != EOF)
		HEAD = AddNode(HEAD, info);
	return HEAD;
}

/*************************************************************
 Name: StoreAccounts
 Purpose: Serialize list to disk
 Parameters: FILE* output (pointer to output file)
			 Node cur (Start of list)
 Return value: none
 Side Effects: Changes content of output file
*************************************************************/
void StoreAccounts(FILE* output, Node cur)
{
	while (cur != NULL)
	{
		fprintf(output, "%s %s %u %s %d %s %.2lf\n",
			cur->firstName,
			cur->lastName,
			cur->state,
			cur->phoneNumber,
			cur->id,
			cur->passwordHash,
			cur->balance);
		cur = cur->next;
	}
}
