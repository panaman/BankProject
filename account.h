#ifndef ACCOUNT_H
#define ACCOUNT_H

typedef enum state {
	AL, AK, AZ, AR,
	CA, CO, CT,
	DC, DE,
	FL,
	GA,
	HI,
	ID, IL, IN, IA,
	KS, KY,
	LA,
	ME, MD, MH, MA, MI, MN, MS, MO, MT,
	NE, NV, NH, NJ, NM, NY, NC, ND,
	OH, OK, OR,
	PA,
	RI,
	SC, SD,
	TN, TX,
	UT,
	VT, VA,
	WA, WV, WI, WY
} State;

typedef struct data
{
	struct data*  next;
	double        balance;
	char          passwordHash[10];
	char          firstName[10];
	char          lastName[10];
	char          phoneNumber[10];
	int           id;
	State         state;
} Data;

typedef Data* Node;

Data CreateData(int id, double, char* fn, char* ln, char* pn, State, long hash);
Node CreateNode(Data);
Node AddNode(Node head, Data newNode);
Node RemoveNode(Node head, int id);
void PrintAccounts(Node HEAD);

#endif
