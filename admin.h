#ifndef ADMIN_H
#define ADMIN_H
#include "account.h"

void ShowLargestAccounts(Node);

void ShowAccountsByLetter(Node, char);

#endif
