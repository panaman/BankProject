#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "account.h"

Node CreateNode(Data info)
{
	Node temp = malloc(sizeof(Data));
	*temp = info;
	temp->next = NULL;
	return temp;
}

Data CreateData(int id, double b, char* fn, char* ln, char* pn, State s, long h)
{
	Data temp;

	strncpy(temp.firstName, fn, 9);
	strncpy(temp.lastName, ln, 9);
	strncpy(temp.phoneNumber, pn, 9);
	//temp.passwordHash = h;
	temp.state = s;
	temp.id = id;
	temp.balance = b;

	return temp;
}

Node AddNode(Node head, Data add)
{
	Node new = CreateNode(add);
	if (head == NULL)
		return new;
	new->next = head;
	return new;
}

void PrintAccounts(Node head)
{
	Node cur = head;

	puts(" ________________________________\n"
		 "|        ID|     First|      Last|\n"
		 "|__________|__________|__________|");
	while (cur != NULL)
	{
		printf("|%10d|%10s|%10s|\n", cur->id, cur->firstName, cur->lastName);
		cur = cur->next;
	}
	puts(" --------------------------------");
}
